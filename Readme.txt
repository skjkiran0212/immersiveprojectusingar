Project Objective 

The project intent is to design and develop Indoor navigation System Lot of navigation apps like Google map are available for navigation but none of them support navigation inside a building by taking into account rooms, corridor and floors. The challenge is to create an app that would show a navigation path in the real world on your mobile device screen. This technology is beneficial to everyday citizens because it allows one to accurately navigate to a specific location in a building they have never been to before, such as government offices, a classroom in a campus, malls/retail store to show customers the items they would want to purchase etc. We are using computer vision and crowdsourcing tools to locate user in building for more accuracy. User is also provided with 3D map of building so he/she can see localise them in complete bulding.
Since we are using personalised pedometry data for user. User's navigation is personalized and more accuracy can be achieved.
These services can also be used in rescue system in building where user can directly ask for nearest fire exit and app will provide path

Required path is navigated accurately and can be used for city wide navigation.
Used for escaping from buildings, in case of emergency situations like fire.


The rapid increase of AR-based indoor navigation applications in different consumer sectors is estimated to experience tremendous growth in the coming years as technologies continue to evolve. As users increasingly become more digital savvy and ready to apply new technologies in their everyday life. Here the work of our project comes into play.

Immersive Project Using AR-Maps  is AUGMENTED REALITY based Indoor Navigation System.

Why Immersive Project Using AR

Suppose any person say Shyam, a senior manager at a tech company. She was looking for an Apple Store, so she went to the Westfield Valley Fair, a midsize mall in the Silicon Valley which is easy to find via Google maps even if you’re not local. It took her 5 minutes to buy a new iPhone. But before that, she spent like 20 minutes walking around to find the store inside the mall. Despite using public maps placed on some type of citylights around the mall, it still could be a challenging task. As Shyam shared the story, I was already digging into the indoor navigation topic.


Challenges of the Project

Indoor navigation is quite different in terms of complexity compared to outdoor navigation. With outdoor navigation, millions of people currently use the technology as it doesn’t require much performance; modern smartphones and even smart watches have built-in GPS and maps.AR indoor navigation technology, on the other hand, is quite complex as it consists of 3 modules which have to be factored in; and these include: Positioning, Mapping, and Rendering.


Technology Stack